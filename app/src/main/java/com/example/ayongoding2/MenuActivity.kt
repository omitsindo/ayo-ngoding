package com.example.ayongoding2

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.course_list.view.*
import kotlinx.android.synthetic.main.course_list.view.courseLayout

class MenuActivity : AppCompatActivity() {

    var courseList = ArrayList<Course>()
    var courseAdapter:CourseAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val actionBar = supportActionBar
        actionBar!!.title = "Course"

        courseList.add(Course("Java", "Beginner", R.drawable.java_logo, 30))
        courseList.add(Course("Python", "Beginner", R.drawable.python1, 60))
        courseList.add(Course("Kotlin", "Beginner", R.drawable.kotlin, 45))
        courseList.add(Course("C++", "Beginner", R.drawable.cpp,80))
        courseList.add(Course("Java", "Beginner", R.drawable.java_logo, 90))
        courseList.add(Course("Python", "Beginner", R.drawable.python1, 100))
        courseList.add(Course("Kotlin", "Beginner", R.drawable.kotlin,10))
        courseList.add(Course("C++", "Beginner", R.drawable.cpp,80))
        courseAdapter = CourseAdapter(this, courseList)

        list.adapter = courseAdapter

        profile.setOnClickListener {
            var intent = Intent(this,ProfileActivity::class.java)
            startActivity(intent)
        }
    }

    class CourseAdapter:BaseAdapter {

        var courseList = ArrayList<Course>()
        var context:Context? = null

        constructor(context:Context, courseList:ArrayList<Course>):super(){
            this.context = context
            this.courseList = courseList
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var course = courseList[position]
            var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var myCourse = inflator.inflate(R.layout.course_list, null)
            myCourse.courseName.text = course.courseName!!
            myCourse.courseType.text = course.courseType!!
            myCourse.logoMateri.setImageResource(course.courseLogo!!)
            myCourse.progressBar.progress = course.progress!!
            myCourse.courseLayout.setOnClickListener {
                if(course.courseName.equals("Java")) {
                    var intent = Intent(context!!, CourseActivity::class.java)
                    intent.putExtra("Name","Java")
                    context!!.startActivity(intent)
                }else if (course.courseName.equals("Kotlin")){
                    var intent = Intent(context!!, CourseActivity::class.java)
                    intent.putExtra("Name","Kotlin")
                    context!!.startActivity(intent)
                }else if (course.courseName.equals("Python")){
                    var intent = Intent(context!!, CourseActivity::class.java)
                    intent.putExtra("Name","Python")
                    context!!.startActivity(intent)
                }else if (course.courseName.equals("C++")){
                    var intent = Intent(context!!, CourseActivity::class.java)
                    intent.putExtra("Name","C++")
                    context!!.startActivity(intent)
                }
            }
            return myCourse
        }

        override fun getItem(position: Int): Any {
            return  courseList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return courseList.size
        }
    }


}
