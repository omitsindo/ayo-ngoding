package com.example.ayongoding2

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var userList = ArrayList<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userList.add(User("Suryawan","Suryawan16","suryawan.arya@outlook.com"))

        val actionBar = supportActionBar
        actionBar!!.title = "Ayo Ngoding"

        bLogin.setOnClickListener {
            for (i in userList.indices){
                if (userList[i].name.equals(txUserName.text.toString())){
                    if (userList[i].password.equals(txPassword.text.toString())){
                        var intent = Intent(this,MenuActivity::class.java)
                        startActivity(intent)
                    }else{
                        txPassword.text.clear()
                        Toast.makeText(this, "Password Salah",Toast.LENGTH_SHORT).show()
                    }
                }else{
                    txUserName.text.clear()
                    Toast.makeText(this, "User Name Salah",Toast.LENGTH_SHORT).show()
                }
            }
        }
        bRegister.setOnClickListener {
            var intent = Intent(this,RegistrationActivity::class.java)
            startActivity(intent)
        }
    }
}
