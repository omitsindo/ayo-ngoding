package com.example.ayongoding2

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_soal.*

class SoalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_soal)

        val actionBar = supportActionBar
        actionBar!!.title = "Java"

        var s = ArrayList<Soal>()

        pilihanA.setBackgroundColor(Color.LTGRAY)
        pilihanB.setBackgroundColor(Color.LTGRAY)
        pilihanC.setBackgroundColor(Color.LTGRAY)
        pilihanD.setBackgroundColor(Color.LTGRAY)

        s.add(Soal("System.out.println() digunakan untuk menampilkan data yang ada" +
                "\ndiantara tanda \"()\", apa yang akan ditamppilkan dari perintah ini" +
                "\nSystem.out.println(\"Hallo\")",
                "C. Hallo",
                "A. \"Hallo\"",
                "B. hallo",
                "C. Hallo",
                "D. \"hallo\"",
                "Jika yang dimasukan kedalam tanda \"()\" pada System.out.println adalah String maka" +
                        "\ndata yang dimasukan harus diapit oleh tanda \" namun pada saat dijalankan tanda \" tidak akan muncul"))
        s.add(Soal("System.out.println() digunakan untuk menampilkan data yang ada" +
                "\ndiantara tanda \"()\", apa yang akan ditamppilkan dari perintah ini" +
                "\nSystem.out.println(3 + 2)",
                "D. 5",
                "A. 3 + 2",
                "B. Error",
                "C. \"3 + 2\"",
                "D. 5",
                "Jika yang dimasukan kedalam tanda \"()\" pada System.out.println adalah String maka" +
                        "\ndata yang dimasukan harus diapit oleh tanda \" namun pada saat dijalankan tanda \" tidak akan muncul"))
        s.add(Soal("System.out.println() digunakan untuk menampilkan data yang ada" +
                "\ndiantara tanda \"()\", apa yang akan ditamppilkan dari perintah ini" +
                "\nSystem.out.println(\"3\" + 2)",
                "A. 32",
                "A. 32",
                "B. Error",
                "C. \"3 + 2\"",
                "D. 5",
                "Jika yang dimasukan kedalam tanda \"()\" pada System.out.println adalah String maka" +
                        "\ndata yang dimasukan harus diapit oleh tanda \" namun pada saat dijalankan tanda \" tidak akan muncul"))

        var jawabanUser:String? = null
        var count:Int = 0
        var possision:Int = 0
        var noSoal:Int = 1

        soal.text = s[possision].soal
        pilihanA.text = s[possision].pilihanA
        pilihanB.text = s[possision].pilihanB
        pilihanC.text = s[possision].pilihanC
        pilihanD.text = s[possision].pilihanD
        penjelasan.text = s[possision].penjelasan
        nomorSoal.text = noSoal.toString()
        pilihanA.setOnClickListener {
            count = 1
            pilihanB.setBackgroundColor(Color.LTGRAY)
            pilihanC.setBackgroundColor(Color.LTGRAY)
            pilihanD.setBackgroundColor(Color.LTGRAY)
            pilihanA.setBackgroundColor(Color.GREEN)
            jawabanUser = pilihanA.text.toString()
        }
        pilihanB.setOnClickListener {
            count = 1
            pilihanA.setBackgroundColor(Color.LTGRAY)
            pilihanC.setBackgroundColor(Color.LTGRAY)
            pilihanD.setBackgroundColor(Color.LTGRAY)
            pilihanB.setBackgroundColor(Color.GREEN)
            jawabanUser = pilihanB.text.toString()
        }
        pilihanC.setOnClickListener {
            count = 1
            pilihanA.setBackgroundColor(Color.LTGRAY)
            pilihanB.setBackgroundColor(Color.LTGRAY)
            pilihanD.setBackgroundColor(Color.LTGRAY)
            pilihanC.setBackgroundColor(Color.GREEN)
            jawabanUser = pilihanC.text.toString()
        }
        pilihanD.setOnClickListener {
            count = 1
            pilihanA.setBackgroundColor(Color.LTGRAY)
            pilihanB.setBackgroundColor(Color.LTGRAY)
            pilihanC.setBackgroundColor(Color.LTGRAY)
            pilihanD.setBackgroundColor(Color.GREEN)
            jawabanUser = pilihanD.text.toString()
        }

        jawab.setOnClickListener {
            if(count != 0){
                if(jawabanUser.equals(s[possision].jawaban)) {
                    Toast.makeText(this, "Jawaban Anda Benar", Toast.LENGTH_SHORT).show()
                    possision++
                    noSoal++
                    pilihanA.setBackgroundColor(Color.LTGRAY)
                    pilihanB.setBackgroundColor(Color.LTGRAY)
                    pilihanC.setBackgroundColor(Color.LTGRAY)
                    pilihanD.setBackgroundColor(Color.LTGRAY)
                    nomorSoal.text = noSoal.toString()
                    soal.text = s[possision].soal
                    pilihanA.text = s[possision].pilihanA
                    pilihanB.text = s[possision].pilihanB
                    pilihanC.text = s[possision].pilihanC
                    pilihanD.text = s[possision].pilihanD
                    penjelasan.text = s[possision].penjelasan
                }else{
                    penjelasan.setVisibility(View.VISIBLE)
                    pilihanA.setVisibility(View.INVISIBLE)
                    pilihanB.setVisibility(View.INVISIBLE)
                    pilihanC.setVisibility(View.INVISIBLE)
                    pilihanD.setVisibility(View.INVISIBLE)
                    Toast.makeText(this, "Jawaban Anda Salah",Toast.LENGTH_SHORT).show()
                    jawab.text = "Lanjut"
                }
            }else{
                Toast.makeText(this, "Anda belum memilih jawaban", Toast.LENGTH_SHORT).show()
            }
        }

        for(i in s.indices){

        }
    }
}
