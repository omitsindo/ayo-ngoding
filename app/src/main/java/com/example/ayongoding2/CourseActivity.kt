package com.example.ayongoding2

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.activity_course.*
import kotlinx.android.synthetic.main.java.view.*

class CourseActivity : AppCompatActivity() {

    var listMateri = ArrayList<Materi>()
    var adapterMateri:AdapterMateri? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course)
        var name = intent.getStringExtra("Name")
        judul.text = name

        if (name.equals("Java")){
            logoMateri.setImageResource(R.drawable.java_logo)
            listMateri.add(Materi("Pengenalan",50))
            listMateri.add(Materi("Variable dan Tipe Data",50))
            listMateri.add(Materi("Logika",50))
            listMateri.add(Materi("Arrays",50))
            listMateri.add(Materi("Functions",50))
            listMateri.add(Materi("Pengulangan",50))
            listMateri.add(Materi("Collection",50))
            adapterMateri = AdapterMateri(this,listMateri)
            course.adapter = adapterMateri

        }else if (name.equals("Python")){
            logoMateri.setImageResource(R.drawable.python1)
            listMateri.add(Materi("Pengenalan",50))
            listMateri.add(Materi("Variable dan Tipe Data",50))
            listMateri.add(Materi("Logika",50))
            listMateri.add(Materi("Arrays",50))
            listMateri.add(Materi("Functions",50))
            listMateri.add(Materi("Pengulangan",50))
            adapterMateri = AdapterMateri(this,listMateri)
            course.adapter = adapterMateri
        }else if (name.equals("Kotlin")){
            logoMateri.setImageResource(R.drawable.kotlin)
            listMateri.add(Materi("Pengenalan",50))
            listMateri.add(Materi("Variable dan Tipe Data",50))
            listMateri.add(Materi("Logika",50))
            listMateri.add(Materi("Arrays",50))
            listMateri.add(Materi("Functions",50))
            listMateri.add(Materi("Pengulangan",50))
            adapterMateri = AdapterMateri(this,listMateri)
            course.adapter = adapterMateri
        }else if(name.equals("C++")){
            logoMateri.setImageResource(R.drawable.cpp)
            listMateri.add(Materi("Pengenalan",50))
            listMateri.add(Materi("Variable dan Tipe Data",50))
            listMateri.add(Materi("Logika",50))
            listMateri.add(Materi("Arrays",50))
            listMateri.add(Materi("Functions",50))
            listMateri.add(Materi("Pengulangan",50))
            adapterMateri = AdapterMateri(this,listMateri)
            course.adapter = adapterMateri

        }
    }

    class AdapterMateri:BaseAdapter {

        var listMateri = ArrayList<Materi>()
        var context:Context? = null

        constructor(context:Context, materi:ArrayList<Materi>):super(){
            this.context = context
            this.listMateri = materi
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var materi = listMateri[position]
            var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var myMateri = inflator.inflate(R.layout.java, null)
            myMateri.judul.text = materi.judulMateri!!
            myMateri.progress.progress = materi.progress!!
            myMateri.setOnClickListener {
                if (materi.judulMateri.equals("Pengenalan")){
                    var intent = Intent(context!!, SoalActivity::class.java)
                    context!!.startActivity(intent)
                }
            }
            return myMateri
        }

        override fun getItem(position: Int): Any {
            return listMateri[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return listMateri.size
        }
    }
}
