package com.example.ayongoding2

class UserCourse{
    var judul:String? = null
    var progress:Int? = null

    constructor(){
        judul = null
        progress = null
    }

    constructor(judul:String, progress:Int){
        this.judul = judul
        this.progress = progress
    }
}