package com.example.ayongoding2

class Materi{
    var judulMateri:String? = null
    var progress:Int? = null

    constructor(){
        judulMateri = null
        progress = null
    }

    constructor(judulMateri:String, progress:Int){
        this.judulMateri = judulMateri
        this.progress = progress
    }
}