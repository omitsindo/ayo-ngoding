package com.example.ayongoding2

import android.support.v4.app.INotificationSideChannel

class Course{
    var courseName:String? = null
    var courseType:String? = null
    var courseLogo:Int? = null
    var progress:Int? = null

    constructor(){
        courseName = null
        courseType = null
        courseLogo = null
        progress = null
    }
    constructor(courseName:String, courseType:String, courseLogo:Int, progress:Int){
        this.courseName = courseName
        this.courseType =courseType
        this.courseLogo = courseLogo
        this.progress = progress
    }
}