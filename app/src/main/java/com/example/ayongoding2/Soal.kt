package com.example.ayongoding2

class Soal{
    var soal:String? = null
    var jawaban:String? = null
    var pilihanA:String? = null
    var pilihanB:String? = null
    var pilihanC:String? = null
    var pilihanD:String? = null
    var penjelasan:String? = null

    constructor(){
        soal =  null
        jawaban = null
        pilihanA = null
        pilihanB = null
        pilihanC = null
        pilihanD = null
        penjelasan = null
    }

    constructor(soal:String, jawaban:String, pilihanA:String, pilihanB:String, pilihanC:String, pilihanD: String, penjelasan:String){
        this.soal = soal
        this.jawaban = jawaban
        this.pilihanA = pilihanA
        this.pilihanB = pilihanB
        this.pilihanC = pilihanC
        this.pilihanD = pilihanD
        this.penjelasan = penjelasan

    }
}