package com.example.ayongoding2

class User{
    var name:String? = null
    var password:String? = null
    var email:String? = null

    constructor(){
        name = null
        password = null
        email = null
    }
    constructor(name:String,password:String,email:String){
        this.name = name
        this.password = password
        this.email = email
    }

    fun GetName():String{
        return name!!
    }
    fun GetPassword():String{
        return password!!
    }
    fun GetEmail():String{
        return email!!
    }
}